[![build status](https://gitlab.com/norad/api/badges/master/build.svg)](https://gitlab.com/norad/api/commits/master)
[![coverage report](https://gitlab.com/norad/api/badges/master/coverage.svg)](https://gitlab.com/norad/api/commits/master)

# Norad API

The Norad API is the central component of the Norad Security Assessment as a
Service Offering. It maintains assets, results, test configuration, and test
results. It is also responsible for scheduling security tests.

## Getting Started

The easiest way to get up and running is to provision a
[Norad Dev Box](https://gitlab.com/norad/dev-box).

## Loading Remote Security Tests

The utility script found at ```utilities/load_security_containers_from_repo.rb``` can be used to
pull in remote Security Test data. It currently supports loading in an artifact from a Gitlab CI
build.

```sh
$> bin/rails runner utilities/load_security_containers_from_repo.rb <url_to_artifact>
```

## Startup Options

The default behavior is to validate that machines with a [RFC1918](https://tools.ietf.org/html/rfc1918)
IP address cannot be scanned unless there is a configured relay. In
the event that you need to scan machines targeting RFC1918 addresses without a relay,
start Norad API with the environment variable `RFC1918_RELAY_OPTIONAL` set to `true`.

## Running Security Tests

The API uses Resque workers for scheduling security tests with a Docker
instance. The simplest way to get your resque workers up and running is to start
them with the following command:

```sh
$> VERBOSE=1 QUEUE=* rake environment resque:work
```

## Testing Airbrake

You must either be in the Rails production environment, or add `gem 'airbrake'`
to the Gemfile under development, and comment out `if Rails.env.production?`
[here](https://gitlab.com/norad/api/blob/96c81b38e31db4/config/initializers/errbit.rb#L3).
After that, setup a local Errbit setup (or even just a local simple dummy http server),
and point [the host in the config](https://gitlab.com/norad/api/blob/96c81b38e31db4/config/initializers/errbit.rb#L6)
to it.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of
conduct and the process for submitting merge requests.
