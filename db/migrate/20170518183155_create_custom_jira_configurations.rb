class CreateCustomJiraConfigurations < ActiveRecord::Migration[5.0]
  def change
    create_table :custom_jira_configurations do |t|
      t.string :title
      t.string :site_url, null: false
      t.string :project_key, null: false
      t.string :username_encrypted
      t.string :password_encrypted
      t.belongs_to :result_export_queue, index: true
      t.timestamps
    end

    add_foreign_key :custom_jira_configurations, :result_export_queues, on_delete: :cascade
  end
end
