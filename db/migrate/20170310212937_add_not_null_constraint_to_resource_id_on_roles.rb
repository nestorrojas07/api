class AddNotNullConstraintToResourceIdOnRoles < ActiveRecord::Migration[5.0]
  def change
    change_column_null :roles, :resource_id, false
  end
end
