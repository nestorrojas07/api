class RenameBugTrackingProjectsToResultExportQueues < ActiveRecord::Migration[5.0]
  def up
    drop_table :bug_tracking_projects

    create_table :result_export_queues do |t|
      t.belongs_to :organization
      t.string :created_by, null: false
      t.string :type, null: false, index: true
      t.boolean :auto_sync, null: false, default: false
      t.timestamps
    end
    add_foreign_key :result_export_queues, :organizations, on_delete: :cascade
  end

  def down
    drop_table :result_export_queues

    create_table :bug_tracking_projects do |t|
      t.belongs_to :organization
      t.string :title
      t.integer :bug_tracker
      t.string :site_url
      t.string :project_key
      t.string :username
      t.string :password_encrypted
      t.string :created_by
      t.datetime :created_at
      t.boolean :login_validated, null: false, default: false
      t.boolean :bugs_created, null: false, default: false
      t.boolean :active_project

      t.timestamps
    end
    add_foreign_key :bug_tracking_projects, :organizations, on_delete: :cascade
  end
end
