class RemoveAgentCommandIdFromWhiteBoxAssessments < ActiveRecord::Migration[5.0]
  def change
    remove_column :white_box_assessments, :agent_command_id, :integer
  end
end
