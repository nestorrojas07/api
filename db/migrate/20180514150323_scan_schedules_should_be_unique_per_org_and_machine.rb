class ScanSchedulesShouldBeUniquePerOrgAndMachine < ActiveRecord::Migration[5.0]
  def up
    execute <<~SQL
      DELETE FROM organization_scan_schedules WHERE id IN (
        SELECT id
        FROM
          organization_scan_schedules AS rows_to_delete
        INNER JOIN (
          SELECT organization_id, MAX(created_at) AS max_created_at
          FROM organization_scan_schedules
          GROUP BY organization_id
        ) AS created_at_max_dictionary
        ON
          rows_to_delete.organization_id = created_at_max_dictionary.organization_id
        WHERE
          rows_to_delete.created_at < created_at_max_dictionary.max_created_at
      )
    SQL

    execute <<~SQL
      DELETE FROM machine_scan_schedules WHERE id IN (
        SELECT id
        FROM
          machine_scan_schedules AS rows_to_delete
        INNER JOIN (
          SELECT machine_id, MAX(created_at) AS max_created_at
          FROM machine_scan_schedules
          GROUP BY machine_id
        ) AS created_at_max_dictionary
        ON
          rows_to_delete.machine_id = created_at_max_dictionary.machine_id
        WHERE
          rows_to_delete.created_at < created_at_max_dictionary.max_created_at
      )
    SQL
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
