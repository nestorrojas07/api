class AddConstraintsToScanSchedulesTables < ActiveRecord::Migration[5.0]
  def change
    remove_index :machine_scan_schedules, :machine_id
    remove_index :organization_scan_schedules, :organization_id

    add_index :machine_scan_schedules, :machine_id, unique: true, using: :btree
    add_index :organization_scan_schedules, :organization_id, unique: true, using: :btree

    change_column_null :machine_scan_schedules, :machine_id, false
    change_column_null :organization_scan_schedules, :organization_id, false
  end
end
