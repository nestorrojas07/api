class AddAllowBruteForceColumnToServices < ActiveRecord::Migration[4.2]
  def change
    add_column :services, :allow_brute_force, :boolean, default: false, null: false
  end
end
