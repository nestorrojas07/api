class ChangeJsonToJsonb < ActiveRecord::Migration[5.0]
  def change
    # Postgres does not have an operation to test equality with "json" types,
    # but jsonb works just fine. Rails won't know the difference.
    # See https://github.com/activerecord-hackery/ransack/issues/453
    change_column :security_containers, :default_config, :jsonb
  end
end
