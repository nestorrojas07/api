class CreateOrganizationScanSchedules < ActiveRecord::Migration[4.2]
  def change
    create_table :organization_scan_schedules do |t|
      t.references :organization, index: true
      t.integer :period, default: 0, null: false
      t.string :at, null: false

      t.timestamps null: false
    end
    add_foreign_key :organization_scan_schedules, :organizations, on_delete: :cascade
  end
end
