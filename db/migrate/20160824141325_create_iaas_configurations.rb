class CreateIaasConfigurations < ActiveRecord::Migration[4.2]
  def change
    create_table :iaas_configurations do |t|
      t.integer :provider, null: false
      t.string :user_encrypted, null: true # required by Vault
      t.string :key_encrypted, null: true # required by Vault
      t.string :region
      t.string :project
      t.string :auth_url
      t.references :organization, null: false

      t.timestamps null: false
    end
    add_index :iaas_configurations, :organization_id, unique: true
    add_foreign_key :iaas_configurations, :organizations, on_delete: :cascade
  end
end
