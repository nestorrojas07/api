class RemoveOrganizationErrorTypeOnMachineConnectivityChecks < ActiveRecord::Migration[5.0]
  def up
    remove_column :machine_connectivity_checks, :organization_error_type
  end

  def down
    add_column :machine_connectivity_checks, :organization_error_type, :string
    execute set_org_error_type_sql('PingConnectivityCheck', 'UnableToPingMachineError')
    execute set_org_error_type_sql('SshConnectivityCheck', 'UnableToSshToMachineError')
    change_column_null :machine_connectivity_checks, :organization_error_type, false
  end

  private

  def set_org_error_type_sql(type, org_error_type)
    <<~SQL
      UPDATE machine_connectivity_checks
      SET organization_error_type = '#{org_error_type}'
      WHERE type = '#{type}';
    SQL
  end
end
