class AddNullConstraintToNameColumnForMachines < ActiveRecord::Migration[4.2]
  def up
    change_column_null :machines, :name, false
  end

  def down
    change_column_null :machines, :name, true
  end
end
