# frozen_string_literal: true

module NoradScanBuilder
  class MachineScanJob
    include ScanJobUtilities

    attr_reader :scans, :container_id, :organization, :dc
    attr_accessor :secret

    def initialize(machine, container, command)
      @organization = machine.organization
      @dc = command
      @container_id = container.id
      @scans = create_scans(container, machine)
    end

    private

    def create_secret
      SecurityContainerSecret.create!
    end

    def relevant_services(container, machine)
      machine.services.testable_by(container)
    end

    def create_scans(container, machine)
      container.whole_host? ? create_whole_host_scan(container, machine) : create_service_scans(container, machine)
    end

    def create_whole_host_scan(container, machine)
      self.secret = create_secret
      [WholeHostScan.new(container, secret.id, dc, machine: machine)]
    end

    def create_service_scans(container, machine)
      scannable_services = relevant_services(container, machine)
      return [] if scannable_services.empty?
      self.secret = create_secret
      scannable_services.map do |s|
        ServiceScan.new(container, secret.id, dc, service: s, machine: machine)
      end
    end
  end
end
