# frozen_string_literal: true

# == Schema Information
#
# Table name: docker_relays
#
#  id                            :integer          not null, primary key
#  organization_id               :integer          not null
#  public_key                    :text             not null
#  queue_name                    :string           not null
#  state                         :integer          default("online"), not null
#  last_heartbeat                :datetime         not null
#  verified                      :boolean          default(FALSE), not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  key_signature                 :string           not null
#  file_encryption_key_encrypted :string
#  last_reported_version         :string
#  outdated                      :boolean          default(FALSE), not null
#
# Indexes
#
#  index_docker_relays_on_organization_id  (organization_id)
#  index_docker_relays_on_outdated         (outdated)
#  index_docker_relays_on_queue_name       (queue_name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_b84167028c  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :docker_relay do
    organization
    public_key { Base64.encode64(OpenSSL::PKey::RSA.new(4096).public_key.to_pem) }
    verified false

    factory :online_relay do
      state 'online'
    end

    factory :offline_relay do
      state 'offline'
    end
  end
end
