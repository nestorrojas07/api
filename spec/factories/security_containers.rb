# frozen_string_literal: true

# == Schema Information
#
# Table name: security_containers
#
#  id                          :integer          not null, primary key
#  name                        :string           not null
#  category                    :integer          default("whitebox"), not null
#  prog_args                   :string           not null
#  default_config              :jsonb
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  multi_host                  :boolean          default(FALSE), not null
#  test_types                  :string           default([]), not null, is an Array
#  configurable                :boolean          default(FALSE), not null
#  application_type_id         :integer
#  help_url                    :string
#  security_test_repository_id :integer          not null
#
# Indexes
#
#  index_security_containers_on_application_type_id          (application_type_id)
#  index_security_containers_on_s_t_r_id_and_name            (security_test_repository_id,name) UNIQUE
#  index_security_containers_on_security_test_repository_id  (security_test_repository_id)
#
# Foreign Keys
#
#  fk_rails_6ec7669ac9  (application_type_id => application_types.id)
#  fk_rails_c5578062aa  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :security_container, aliases: [:container] do
    name { "factory-security-container-#{SecureRandom.hex(8)}" }
    prog_args '%{target}'
    default_config { {} }
    category :blackbox
    multi_host false
    test_types ['whole_host']
    configurable false

    association :security_test_repository, factory: :official_repository

    factory :private_container do
      association :security_test_repository, factory: :private_repository
    end

    factory :whitebox_container do
      category :whitebox
      prog_args '%{ssh_user} %{ssh_key} %{target}'

      factory :container_with_application_type do
        application_type_id { create(:application_type).id }
      end
    end

    factory :service_test do
      prog_args '%{target} -p %{port}'
      test_types ['ssl_crypto']
    end

    factory :machine_test do
      prog_args '%{target}'
      test_types ['whole_host']
    end

    factory :multi_host_test do
      prog_args '%{target}'
      multi_host true
      test_types ['whole_host']
    end
  end
end
