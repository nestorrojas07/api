# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Service Discovery updates', type: :request do
  include NoradControllerTestHelpers

  describe 'require signed requests by the discovery container' do
    before :each do
      @discovery = create :service_discovery
      @discovery.start!
    end

    it 'requires the request to be signed' do
      payload = { timestamp: Time.now.to_f.to_s, service_discovery: { error_message: '' } }
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, payload.to_json)
      signed_request :put, v1_service_discovery_path(@discovery), payload, 'junk'
      expect(response.status).to eq(401)
      signed_request :put, v1_service_discovery_path(@discovery), payload, sig
      expect(response.status).to eq(200)
    end

    it 'moves the discovery to failed state if an error message is present' do
      payload = { timestamp: Time.now.to_f.to_s, service_discovery: { error_message: 'uh oh' } }
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, payload.to_json)
      signed_request :put, v1_service_discovery_path(@discovery), payload, sig
      expect(@discovery.reload.state).to eq('failed')
    end

    it 'responds with a 422 if the job takes so long its secret is deleted' do
      payload = {
        timestamp: Time.now.to_f.to_s,
        service_discovery: { error_message: 'something' }
      }
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, payload.to_json)
      @discovery.container_secret.update_attributes(secret: nil)
      signed_request :put, v1_service_discovery_path(@discovery), payload, sig

      assert_response 422
      expect(response_body['errors']['base']).to eq(['Secret expired'])
    end

    it 'moves the discovery to a completed state if no error message is present' do
      payload = { timestamp: Time.now.to_f.to_s, service_discovery: { error_message: '' } }
      sig = OpenSSL::HMAC.hexdigest('sha256', @discovery.shared_secret, payload.to_json)
      signed_request :put, v1_service_discovery_path(@discovery), payload, sig
      expect(@discovery.reload.state).to eq('completed')
    end
  end
end
