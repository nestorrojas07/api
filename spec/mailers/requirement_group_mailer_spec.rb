# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RequirementGroupMailer, type: :mailer do
  context 'when a requirement group is deleted' do
    it 'sends the email to all organization admins' do
      user = generate_user_with_orgs_with_admins

      expect do
        RequirementGroupMailer.deleted('group-name', user).deliver_now
      end.to change(ActionMailer::Base.deliveries, :count).by(1)

      email = ActionMailer::Base.deliveries.last

      expect(email.to.to_set).to eq(['admin1@test.com', 'admin2@test.com', user.email].to_set)

      expect(email.subject).to eq(
        "#{ApplicationMailer::SUBJECT_PREFIX} A Requirement Group is no longer available."
      )
      expect(email.body.to_s.strip).to eq(
        'The Requirement Group "group-name" is no longer available. It has been deleted.'
      )
    end

    private

    def generate_user_with_orgs_with_admins
      user = create(:user)
      organization1 = FactoryBot.create(:organization)
      organization2 = FactoryBot.create(:organization)
      user.organizations << organization1
      user.organizations << organization2
      create(:user, email: 'admin1@test.com').add_role(:organization_admin, organization1)
      create(:user, email: 'admin2@test.com').add_role(:organization_admin, organization2)

      user
    end
  end
end
