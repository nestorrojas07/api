# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_job_utilities'

RSpec.describe NoradScanBuilder::MachineScanJob, with_resque_doubled: true do
  let(:machine) { build_stubbed(:machine) }
  let(:container) { build_stubbed(:security_container) }
  let(:secret) { build_stubbed(:security_container_secret) }
  let(:command) { build_stubbed(:docker_command) }

  describe 'instantiation' do
    describe 'secret creation' do
      before(:each) do
        allow(NoradScanBuilder::WholeHostScan).to receive(:new)
        allow(NoradScanBuilder::ServiceScan).to receive(:new)
      end

      it 'creates a container secret when running a whole host scan' do
        allow(container).to receive(:whole_host?).and_return(true)
        expect(SecurityContainerSecret).to receive(:create!).and_return(secret)
        described_class.new(machine, container, command)
      end

      it 'does not create a container secret when there are no relevant services to scan' do
        allow(container).to receive(:whole_host?).and_return(false)
        expect(SecurityContainerSecret).to_not receive(:create!)
        described_class.new(machine, container, command)
      end

      it 'creates a container secret when there are relevant services to scan' do
        allow(container).to receive(:whole_host?).and_return(false)
        service = build_stubbed(:service, machine: machine)
        allow(machine).to receive_message_chain(:services, :testable_by).and_return([service])
        expect(SecurityContainerSecret).to receive(:create!).and_return(secret)
        described_class.new(machine, container, command)
      end
    end

    describe 'scan creation' do
      before(:each) { allow(SecurityContainerSecret).to receive(:create!).and_return(secret) }

      it 'creates whole host scans if the test targets the host' do
        container = build_stubbed(:machine_test)
        expect(NoradScanBuilder::WholeHostScan).to receive(:new)
        described_class.new(machine, container, command)
      end

      it 'creates service scans if the test targets a service' do
        container = build_stubbed(:service_test)
        service = build_stubbed(:service)
        services = double
        allow(machine).to receive(:services).and_return(services)
        allow(services).to receive(:testable_by).and_return([service])

        expect(NoradScanBuilder::ServiceScan).to receive(:new)
        described_class.new(machine, container, command)
      end
    end
  end

  describe 'instance methods' do
    include_examples 'Scan Job Utilities' do
      let(:machine) { create(:machine) }
      let(:organization) { machine.organization }
      let(:scan_job) { described_class.new(machine, container, command) }
    end
  end
end
