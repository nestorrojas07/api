# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe DestroyExpiredRecordsJob, type: :job, with_resque_doubled: true do
  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  shared_examples_for 'a Perishable Job' do
    it 'looks for expired MachineConnectivityChecks to destroy' do
      expect(klass).to receive(:expired).and_call_original
      perform_enqueued_jobs { job }
    end

    it 'sends the destroy_all message to the return value of MachineConnectivityCheck.stale' do
      expect(klass).to receive(:expired).and_return(klass.where(id: record.id))
      expect do
        perform_enqueued_jobs { job }
      end.to change(klass, :count).by(-1)
    end
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'
  it_behaves_like 'a Perishable Job' do
    let(:klass) { MachineConnectivityCheck }
    let(:record) { create :expired_machine_connectivity_check }
  end
  it_behaves_like 'a Perishable Job' do
    let(:klass) { SecurityContainerSecret }
    let(:record) { create :expired_security_container_secret }
  end
end
