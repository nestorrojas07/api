# frozen_string_literal: true

# == Schema Information
#
# Table name: custom_jira_configurations
#
#  id                     :integer          not null, primary key
#  title                  :string
#  site_url               :string           not null
#  project_key            :string           not null
#  username_encrypted     :string
#  password_encrypted     :string
#  result_export_queue_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_custom_jira_configurations_on_result_export_queue_id  (result_export_queue_id)
#
# Foreign Keys
#
#  fk_rails_b1be8cc432  (result_export_queue_id => result_export_queues.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe CustomJiraConfiguration, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:project_key) }

    it 'is invalid without a valid url' do
      config = described_class.new(site_url: 'abcdef')
      config.valid?
      expect(config.errors[:site_url]).to include('must be a valid url')
      config.site_url = %r{https?://\S+}.random_example
      config.valid?
      expect(config.errors[:site_url]).to match_array([])
    end

    it 'is invalid with a blank site_url' do
      config = described_class.new(site_url: nil)
      config.valid?
      expect(config.errors[:site_url]).to include('must be a valid url')
    end
  end

  describe 'associations' do
    it { should belong_to(:jira_export_queue) }
  end

  describe 'instance methods' do
    let(:config) { create(:custom_jira_configuration) }

    describe 'attributes' do
      it 'includes the decrypted username and password' do
        expect(config.attributes['username']).not_to be(nil)
        expect(config.attributes['password']).not_to be(nil)
      end
    end
  end
end
