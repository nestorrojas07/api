# frozen_string_literal: true

require 'rails_helper'
require 'models/shared_examples/ignore_scope'

RSpec.describe OrganizationWithLatestResults do
  let(:organization) { create :organization }
  let(:sectest) { create :security_container }
  let(:machine1) { create(:machine, organization: organization) }
  let(:machine2) { create(:machine, organization: organization) }
  let(:sc_attrs) { [{ security_container_id: sectest.id }] }

  it 'returns the latest results for all machines in the organization via #latest_results' do
    org_command = create :docker_command, commandable: organization, scan_containers_attributes: sc_attrs
    assessment1, assessment2 = [machine1, machine2].map do |xmach|
      create :assessment, docker_command: org_command, machine: xmach, security_container: sectest
    end
    _, assessment2_result = [assessment1, assessment2].map do |xassmt|
      create :result, assessment: xassmt
    end
    machine1_command = create :docker_command, commandable: machine1, scan_containers_attributes: sc_attrs
    assessment3 = create :assessment, docker_command: machine1_command, machine: machine1, security_container: sectest
    assessment3_result = create :result, assessment: assessment3

    decorated_organization = described_class.new(organization)

    expected_results = Result.where(id: [assessment2_result, assessment3_result])
    expect(decorated_organization.latest_results).to match_array(expected_results)
  end

  it 'handles no docker commands having ever been run against the organization' do
    machine1_command = create :docker_command, commandable: machine1, scan_containers_attributes: sc_attrs
    machine2_command = create :docker_command, commandable: machine2, scan_containers_attributes: sc_attrs
    assessment1 = create :assessment, docker_command: machine1_command, machine: machine1, security_container: sectest
    assessment2 = create :assessment, docker_command: machine2_command, machine: machine2, security_container: sectest

    assessment1_result = create :result, assessment: assessment1
    assessment2_result = create :result, assessment: assessment2

    decorated_organization = described_class.new(organization)

    expected_results = Result.where(id: [assessment2_result, assessment1_result])
    expect(decorated_organization.latest_results).to match_array(expected_results)
  end
end
