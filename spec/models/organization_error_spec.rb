# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe OrganizationError, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:organization).with_message('must exist') }
  end

  context 'maintaining associations' do
    it { should belong_to(:organization) }
  end

  context 'creating and removing errors' do
    let(:organization) { create :organization }
    let(:errable) { create :machine }

    context '::create_error' do
      before :each do
        @error = create :organization_error, errable: errable, organization: organization
      end

      it 'returns the existing error when org, type, and errable matches' do
        expect { OrganizationError.create_error(organization, errable) }.not_to change(OrganizationError, :count)
      end
    end

    context '::remove_error' do
      before :each do
        5.times { create :organization_error, organization: organization, errable: create(:machine) }
        @error = create :unable_to_ping_machine_error, errable: errable, organization: organization
      end

      it 'only removes error specific to errable and type' do
        expect { OrganizationError.remove_error(organization, errable) }.not_to change(OrganizationError, :count)
        expect { @error.destroy }.to change(OrganizationError, :count).by(-1)
        expect { OrganizationError.create_error(organization, errable) }.to change(OrganizationError, :count).by(1)
      end

      it 'removes all org errors of one type' do
        expect { OrganizationError.remove_error(organization) }.to change(OrganizationError, :count).by(-5)
      end
    end
  end
end
