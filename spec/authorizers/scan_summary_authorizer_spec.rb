# frozen_string_literal: true

require 'rails_helper'

describe ScanSummaryAuthorizer, type: :authorizer do
  context 'scan_summary action' do
    let(:machine_scan_summary) { MachineScanSummary.new }
    let(:org_scan_summary) { OrganizationScanSummary.new }
    let(:dc_machine_summary) { DockerCommandMachineSummary.new }

    before :each do
      @other_user = create :user
      @admin_user = create :user
      @reader_user = create :user
      @org = @admin_user.organizations.first
      @reader_user.add_role :organization_reader, @org
    end

    context 'machine scan summary' do
      it 'try to read from instance level' do
        expect(machine_scan_summary.authorizer).to be_readable_by(@admin_user, in: @org)
        expect(machine_scan_summary.authorizer).to be_readable_by(@reader_user, in: @org)
        expect(machine_scan_summary.authorizer).not_to be_readable_by(@other_user, in: @org)
      end
    end

    context 'org scan summary' do
      it 'try to read from instance level' do
        expect(org_scan_summary.authorizer).to be_readable_by(@admin_user, in: @org)
        expect(org_scan_summary.authorizer).to be_readable_by(@reader_user, in: @org)
        expect(org_scan_summary.authorizer).not_to be_readable_by(@other_user, in: @org)
      end
    end

    context 'dc machine summary' do
      it 'try to read from instance level' do
        expect(dc_machine_summary.authorizer).to be_readable_by(@admin_user, in: @org)
        expect(dc_machine_summary.authorizer).to be_readable_by(@reader_user, in: @org)
        expect(dc_machine_summary.authorizer).not_to be_readable_by(@other_user, in: @org)
      end
    end
  end
end
