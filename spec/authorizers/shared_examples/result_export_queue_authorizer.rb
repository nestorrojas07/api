# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a Result Export Queue Authorizer' do
  before :each do
    @org1 = create :organization
    @admin_user = create :user
    @admin_user.add_role :organization_admin, @org1
    @reader_user = create :user
    @reader_user.add_role :organization_reader, @org1
    @options = { in: @org1 }

    org2 = create :organization
    @admin_user_other = create :user
    @admin_user_other.add_role :organization_admin, org2
    @reader_user_other = create :user
    @reader_user_other.add_role :organization_reader, org2
  end

  context 'result export queue class level authorizers' do
    it 'allows org admins and readers to read lists of result export queues' do
      expect(authorizing_klass.authorizer).to be_readable_by(@admin_user, @options)
      expect(authorizing_klass.authorizer).to be_readable_by(@reader_user, @options)
    end

    it 'does not allow admins and readers from a different organization to read lists of result export queues' do
      expect(authorizing_klass.authorizer).to_not be_readable_by(@admin_user_other, @options)
      expect(authorizing_klass.authorizer).to_not be_readable_by(@reader_user_other, @options)
    end

    it 'allows org admins to create result export queues' do
      expect(authorizing_klass.authorizer).to be_creatable_by(@admin_user, @options)
    end

    it 'does not allow org readers to create result export queues' do
      expect(authorizing_klass.authorizer).to_not be_creatable_by(@reader_user, @options)
    end

    it 'does not allow admins and readers from a different organization to create result export queues' do
      expect(authorizing_klass.authorizer).to_not be_creatable_by(@admin_user_other, @options)
      expect(authorizing_klass.authorizer).to_not be_creatable_by(@reader_user_other, @options)
    end
  end

  context 'result export queue defined' do
    before :each do
      @result_export_queue = create :result_export_queue, organization: @org1
    end

    it 'allows org admins and readers to read a single result export queue' do
      expect(@result_export_queue.authorizer).to be_readable_by(@admin_user)
      expect(@result_export_queue.authorizer).to be_readable_by(@reader_user)
    end

    it 'does not allow admins and readers from a different organization to read a single result export queue' do
      expect(@result_export_queue.authorizer).to_not be_readable_by(@admin_user_other)
      expect(@result_export_queue.authorizer).to_not be_readable_by(@reader_user_other)
    end

    it 'allows org admins to update result export queues' do
      expect(@result_export_queue.authorizer).to be_updatable_by(@admin_user)
    end

    it 'does not allow org readers to update result export queues' do
      expect(@result_export_queue.authorizer).to_not be_updatable_by(@reader_user)
    end

    it 'does not allow admins and readers from a different organization to update result export queues' do
      expect(@result_export_queue.authorizer).to_not be_updatable_by(@admin_user_other)
      expect(@result_export_queue.authorizer).to_not be_updatable_by(@reader_user_other)
    end

    it 'allows org admins to delete result export queues' do
      expect(@result_export_queue.authorizer).to be_deletable_by(@admin_user)
    end

    it 'does not allow org readers to delete result export queues' do
      expect(@result_export_queue.authorizer).to_not be_deletable_by(@reader_user)
    end

    it 'does not allow admins and readers from a different organization to delete result export queues' do
      expect(@result_export_queue.authorizer).to_not be_deletable_by(@admin_user_other)
      expect(@result_export_queue.authorizer).to_not be_deletable_by(@reader_user_other)
    end
  end
end

describe ResultExportQueueAuthorizer, type: :authorizer do
  let(:authorizing_klass) { ResultExportQueue }
end
