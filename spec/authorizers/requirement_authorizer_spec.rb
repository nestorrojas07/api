# frozen_string_literal: true

require 'rails_helper'

describe RequirementAuthorizer, type: :authorizer do
  before :each do
    @requirement_group1 = create :requirement_group
    @requirement = create :requirement, requirement_group: @requirement_group1
    @options = { in: @requirement_group1 }
  end

  context 'as an admin user' do
    before :each do
      @admin_user = create :user
      @admin_user.add_role :requirement_group_admin, @requirement_group1
    end

    it 'try to create' do
      expect(RequirementAuthorizer).to be_creatable_by(@admin_user, @options)
    end

    it 'try to read' do
      expect(RequirementAuthorizer).to be_readable_by(@admin_user)
    end

    it 'try to update' do
      expect(@requirement.authorizer).to be_updatable_by(@admin_user)
    end

    it 'try to destroy' do
      expect(@requirement.authorizer).to be_deletable_by(@admin_user)
    end
  end
end
