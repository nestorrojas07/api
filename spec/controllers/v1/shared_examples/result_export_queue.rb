# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a Result Export Queue Controller' do
  describe 'GET #show' do
    %i[organization_admin organization_reader].each do |role|
      it "gets the requested project for #{role}" do
        @_current_user.add_role role, org
        norad_get :show, id: project_object.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema(schema)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      it 'updates the requested result export queue for organization_admin' do
        @_current_user.add_role :organization_admin, org
        norad_put :update, { id: project_object.to_param }.merge(valid_update_attributes)
        expect(response.status).to eq(200)
        expect(response).to match_response_schema(schema)
      end

      it 'does not update the requested result export queue for organization_reader' do
        @_current_user.add_role :organization_reader, org
        norad_put :update, { id: project_object.to_param }.merge(valid_update_attributes)
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    def create_queue
      norad_post :create, { organization_id: org.id }.merge(valid_create_attributes)
    end

    context 'with valid params' do
      it 'creates a new ResultExportQueue for organization_admin' do
        @_current_user.add_role :organization_admin, org
        expect do
          create_queue
        end.to change(ResultExportQueue, :count).by(1)
        expect(response.status).to eq(201)
        expect(response).to match_response_schema(schema)
      end

      it 'does not create a new ResultExportQueue for organization_reader' do
        @_current_user.add_role :organization_reader, org
        expect do
          create_queue
        end.to change(ResultExportQueue, :count).by(0)
        expect(response.status).to eq(403)
      end

      it 'sets the created_by attribute to the current user' do
        @_current_user.add_role :organization_admin, org
        create_queue
        expect(ResultExportQueue.find(response_body['response']['id']).created_by).to eq(@_current_user.uid)
      end

      context 'InfosecExportQueue', if: described_class.is_a?(V1::InfosecExportQueuesController) do
        it 'redirects to existing queue if already exists' do
          @_current_user.add_role :organization_admin, org
          create_queue
          expect(response).to have_http_status :created
          queue_id = response_body['response']['id']

          create_queue
          expect(response).to have_http_status :not_modified
          expect(response).to redirect_to v1_infosec_export_queue_url(queue_id)
          expect(response_body['response']['id']).to eq queue_id
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested project for organization_admin' do
      @_current_user.add_role :organization_admin, org
      expect(project_object.id).to_not be(nil)
      expect do
        norad_delete :destroy, id: project_object.to_param
      end.to change(ResultExportQueue, :count).by(-1)
      expect(response.status).to eq(204)
    end

    it 'does not destroy the requested project for organization_reader' do
      @_current_user.add_role :organization_reader, org
      expect(project_object.id).to_not be(nil)
      expect do
        norad_delete :destroy, id: project_object.to_param
      end.to change(ResultExportQueue, :count).by(0)
      expect(response.status).to eq(403)
    end
  end
end
