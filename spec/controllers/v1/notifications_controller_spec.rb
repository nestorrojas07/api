# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::NotificationChannelsController, type: :controller do
  describe 'GET #index' do
    before :each do
      @organization = create :organization
      @notification_channel = @organization.notification_channels.first
    end

    context 'as an organization amdin' do
      it 'returns the notification channels associated with an organization' do
        @_current_user.add_role :organization_admin, @organization
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('notification_channels')
      end
    end

    context 'as an organization reader' do
      it 'returns the notification channels associated with an organization' do
        @_current_user.add_role :organization_reader, @organization
        norad_get :index, organization_id: @organization.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('notification_channels')
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @organization = create :organization
      @notification_channel = @organization.notification_channels.first
    end

    context 'as an organization admin' do
      it 'returns the details of a notification_channel' do
        @_current_user.add_role :organization_admin, @organization
        norad_get :show, id: @notification_channel.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('notification_channel')
      end
    end

    context 'as an organization reader' do
      it 'read the details of a specific notification_channel' do
        @_current_user.add_role :organization_reader, @organization
        norad_get :show, id: @notification_channel.id
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('notification_channel')
      end
    end

    context 'as a user outside the organization' do
      it 'cannot read the details of a specific notification_channel' do
        norad_get :show, id: @notification_channel.id
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @organization = create :organization
      @notification_channel = @organization.notification_channels.first
    end

    let(:channel_params) { { id: @notification_channel.to_param } }

    context 'as an organization admin' do
      before :each do
        @_current_user.add_role :organization_admin, @organization
      end

      it 'disables a notification channel for an organization' do
        update_params = channel_params.merge(notification_channel: { enabled: false })
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@notification_channel.reload.enabled).to eq(false)
      end

      it 'enables a notification channel for an organization' do
        @notification_channel.update(enabled: false)
        update_params = channel_params.merge(notification_channel: { enabled: true })
        norad_put :update, update_params
        expect(response.status).to eq(200)
        expect(@notification_channel.reload.enabled).to eq(true)
      end
    end

    context 'as an organization reader' do
      before :each do
        @_current_user.add_role :organization_reader, @organization
      end

      it 'cannot disable notifications for an organization' do
        update_params = channel_params.merge(notification_channel: { enabled: false })
        norad_put :update, update_params
        expect(response.status).to eq(403)
        expect(@notification_channel.reload.enabled).to eq(true)
      end

      it 'cannot enable notifications for an organization' do
        @notification_channel.update(enabled: false)
        update_params = channel_params.merge(notification_channel: { enabled: true })
        norad_put :update, update_params
        expect(response.status).to eq(403)
        expect(@notification_channel.reload.enabled).to eq(false)
      end
    end
  end
end
