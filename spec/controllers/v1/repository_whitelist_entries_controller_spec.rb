# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_r_w_entries_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_27555b3c57  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_c3b0a8cf92  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe V1::RepositoryWhitelistEntriesController, type: :controller do
  before :each do
    @private_repository = create :private_repository
    @public_repository = create :public_repository
    @official_repository = create :official_repository
  end

  describe 'GET #index' do
    before :each do
      org = @_current_user.organizations.first
      create :repository_whitelist_entry, security_test_repository: @private_repository, organization: org
    end

    context 'for admins' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
        @_current_user.add_role :security_test_repository_admin, @public_repository
      end

      context 'private repositories' do
        it 'returns http success' do
          norad_get :index, security_test_repository_id: @private_repository.to_param
          expect(response).to have_http_status(:success)
          expect(response).to match_response_schema('repository_whitelist_entries')
        end
      end
    end

    context 'for unprivileged users' do
      context 'private repositories' do
        it 'returns http forbidden' do
          norad_get :index, security_test_repository_id: @private_repository.to_param
          expect(response).to have_http_status(:forbidden)
        end
      end

      context 'public repositories' do
        it 'returns http success but empty' do
          norad_get :index, security_test_repository_id: @public_repository.to_param
          expect(response).to have_http_status(:success)
          expect(response_body['response']).to be_empty
        end
      end
    end
  end

  describe 'POST #create' do
    let(:private_valid_params) do
      {
        repository_whitelist_entry: { organization_id: @_current_user.organizations.first.id },
        security_test_repository_id: @private_repository.id
      }
    end
    let(:public_valid_params) do
      {
        repository_whitelist_entry: { organization_id: @_current_user.organizations.first.id },
        security_test_repository_id: @public_repository.id
      }
    end
    let(:official_params) do
      {
        repository_whitelist_entry: { organization_id: @_current_user.organizations.first.id },
        security_test_repository_id: @official_repository.id
      }
    end

    context 'for admins' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
        @_current_user.add_role :security_test_repository_admin, @public_repository
      end

      context 'private repositories' do
        it 'creates with valid params' do
          norad_post :create, private_valid_params
          expect(response).to have_http_status(:success)
          expect(response).to match_response_schema('repository_whitelist_entry')
        end

        it 'raises RecordNotFound for missing org_id' do
          expect do
            norad_post :create,
                       security_test_repository_id: @private_repository.to_param,
                       repository_whitelist_entry: { organization_id: nil }
          end.to raise_exception ActiveRecord::RecordNotFound
        end
      end

      context 'public repositories' do
        it 'renders errors for adding official repositories' do
          norad_post :create, official_params
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response_body['errors']['base'])
            .to include "Official Norad Repository can't be modified, removed, or whitelisted"
        end

        it 'returns success' do
          norad_post :create, public_valid_params
          expect(response).to have_http_status(:success)
          expect(response).to match_response_schema('repository_whitelist_entry')
        end

        it 'raises RecordNotFound for missing org_id' do
          expect do
            norad_post :create,
                       security_test_repository_id: @public_repository.to_param,
                       repository_whitelist_entry: { organization_id: nil }
          end.to raise_exception ActiveRecord::RecordNotFound
        end
      end
    end

    context 'for unprivileged users' do
      context 'private repositories' do
        it 'returns http forbidden' do
          norad_post :create, private_valid_params
          expect(response).to have_http_status(:forbidden)
        end
      end

      context 'public repositories' do
        it 'returns success' do
          norad_post :create, public_valid_params
          expect(response).to have_http_status(:success)
        end
      end

      context 'official repositories' do
        it 'returns unprocessable_entity' do
          norad_post :create, official_params
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    shared_examples_for 'a privileged destroy' do
      it 'should destroy' do
        norad_delete :destroy, id: entry.to_param
        expect(response).to have_http_status(:no_content)
      end
    end

    shared_examples_for 'an unprivileged destroy' do
      it 'returns http forbidden' do
        norad_delete :destroy, id: entry.to_param
        expect(response).to have_http_status(:forbidden)
      end
    end

    before :each do
      @private_entry = create :repository_whitelist_entry,
                              organization: @_current_user.organizations.first,
                              security_test_repository: @private_repository
      @public_entry = create :repository_whitelist_entry,
                             organization: @_current_user.organizations.first,
                             security_test_repository: @public_repository
    end

    context 'for admins' do
      before :each do
        @_current_user.add_role :security_test_repository_admin, @private_repository
        @_current_user.add_role :security_test_repository_admin, @public_repository
      end

      context 'private repositories' do
        it_behaves_like('a privileged destroy') { let(:entry) { @private_entry } }
      end

      context 'public repositories' do
        it_behaves_like('a privileged destroy') { let(:entry) { @public_entry } }
      end
    end

    context 'for unprivileged users' do
      context 'private repositories' do
        it_behaves_like('an unprivileged destroy') { let(:entry) { @private_entry } }
      end

      context 'public repositories' do
        it_behaves_like('a privileged destroy') { let(:entry) { @public_entry } }
      end
    end

    context 'for readers' do
      before :each do
        @_current_user.add_role :security_test_repository_reader, @private_repository
      end

      context 'private repositories' do
        it_behaves_like('a privileged destroy') { let(:entry) { @private_entry } }
      end

      context 'public repositories' do
        it_behaves_like('a privileged destroy') { let(:entry) { @public_entry } }
      end
    end
  end
end
