# frozen_string_literal: true

require 'rails_helper'
require 'controllers/v1/shared_examples/result_export_queue'

RSpec.describe V1::JiraExportQueuesController, type: :controller do
  let(:org) { create :organization }

  let(:valid_create_attributes) do
    valid_update_attributes
  end

  let(:invalid_create_attributes) do
    invalid_update_attributes
  end

  let(:valid_update_attributes) do
    { jira_export_queue: attributes_for(:jira_export_queue).merge(valid_nested_attributes) }
  end

  let(:invalid_update_attributes) do
    { jira_export_queue: attributes_for(:jira_export_queue).merge(invalid_nested_attributes) }
  end

  let(:valid_nested_attributes) do
    { custom_jira_configuration_attributes: attributes_for(:custom_jira_configuration) }
  end

  let(:invalid_nested_attributes) do
    { custom_jira_configuration_attributes: attributes_for(:custom_jira_configuration, site_url: 'foo') }
  end

  let(:missing_nested_attributes) do
    attributes_for(:jira_export_queue)
  end

  let(:project_object) { create :jira_export_queue, organization: org }
  let(:schema) { 'jira_export_queue' }

  it_behaves_like 'a Result Export Queue Controller'

  describe 'PUT #update' do
    it 'requires the jira configuration nested attributes to be in the parameters' do
      @_current_user.add_role :organization_admin, org
      expect do
        norad_put :update, id: project_object.to_param, jira_export_queue: missing_nested_attributes
      end.to raise_error(ActionController::ParameterMissing)
    end

    context 'with invalid params' do
      it 'returns 422 if a param is present with an invalid value' do
        @_current_user.add_role :organization_admin, org
        norad_put :update, { id: project_object.to_param }.merge(invalid_update_attributes)
        expect(response.status).to eq(422)
      end
    end
  end

  describe 'POST #create' do
    it 'requires the jira configuration nested attributes to be in the parameters' do
      @_current_user.add_role :organization_admin, org
      expect do
        norad_post :create, organization_id: org.id, jira_export_queue: missing_nested_attributes
      end.to raise_error(ActionController::ParameterMissing)
    end

    context 'with invalid params' do
      it 'does not create a new ResultExportQueue' do
        @_current_user.add_role :organization_admin, org
        expect do
          norad_post :create, { organization_id: org.id }.merge(invalid_create_attributes)
        end.to change(ResultExportQueue, :count).by(0)
        expect(response.status).to eq(422)
      end
    end
  end
end
