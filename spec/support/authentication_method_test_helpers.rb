# frozen_string_literal: true

module AuthenticationMethodTestHelpers
  def mock_auth_type(type)
    case type.to_s
    when 'reverse_proxy'
      set_sso_domain
    else
      unset_sso_domain
    end
  end

  private

  def unset_sso_domain
    ENV['SSO_DOMAIN'] = nil
  end

  def set_sso_domain
    ENV['SSO_DOMAIN'] = 'cisco.com'
  end
end
