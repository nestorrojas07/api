# frozen_string_literal: true

class DockerSwarm
  Docker.url = ENV['SWARM_MASTER_URL']
  DOCKER_NETWORK_NAME = ENV.fetch('DOCKER_NETWORK_NAME')
  DOCKER_DNS_SERVERS = ENV.fetch('DOCKER_DNS_SERVERS')&.split(',')

  class << self
    # rubocop:disable Metrics/ParameterLists
    def queue_container(name, generic_container, args, targets, secret, relay_opts = {})
      container_options = ContainerEnvironmentBuilder.build_options_hash(
        name, generic_container, args, targets, secret
      )
      send_or_start_container(generic_container, container_options, relay_opts)
    end
    # rubocop:enable Metrics/ParameterLists

    def queue_discovery_container(discovery, generic_container, relay_opts = {})
      discovery_environment = ContainerEnvironmentBuilder.build_discovery_environment(discovery)
      container_opts = {
        'Image' => generic_container.full_path,
        'Env' => discovery_environment,
        'Cmd' => [discovery.machine.target_address]
      }
      send_or_start_container(generic_container, container_opts, relay_opts)
    end

    def queue_machine_connectivity_container(check, generic_container, relay_opts = {})
      connectivity_environment = ContainerEnvironmentBuilder.build_connectivity_check_environment(check)
      container_opts = {
        'Image' => generic_container.full_path,
        'Env' => connectivity_environment,
        'Cmd' => check.container_args
      }
      send_or_start_container(generic_container, container_opts, relay_opts)
    end

    private

    def send_or_start_container(generic_container, container_opts, relay_opts = {})
      if relay_opts[:relay_queue]
        # Grab creds from repository before sending to Relay
        send_to_relay(container_opts.merge(generic_container.creds), relay_opts)
      else
        # No need for creds -- the official repository is open
        start_container(generic_container, container_opts)
      end
    end

    def start_container(generic_container, container_options)
      raise(NoradApiError, 'External container requested for Norad Cloud!') unless generic_container.official?
      pull_image(generic_container)
      Rails.logger.debug "Starting container for: #{generic_container.full_path}"
      container_options.merge!(network_options)
      docker_container = Docker::Container.create(container_options)
      docker_container.start
    end

    def send_to_relay(container_options, relay_opts)
      container_options['encoded_relay_private_key'] = relay_opts[:relay_secret]
      statement = { docker_options: container_options }
      Publisher.publish relay_opts[:relay_exchange], statement, relay_opts[:relay_queue]
    end

    # Call this method before starting a container in order to ensure that the node has the latest
    # version of the image you are wanting to run
    def pull_image(generic_container)
      opts = { fromImage: generic_container.full_path }
      creds = generic_container.creds

      # Analogous to `docker pull <image_name>`
      if creds.present?
        Docker::Image.create opts, creds
      else
        Docker::Image.create opts
      end
    end

    def network_options
      { NetworkMode: DOCKER_NETWORK_NAME, Dns: DOCKER_DNS_SERVERS }
    end
  end
end
