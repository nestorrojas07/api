# frozen_string_literal: true

module V1
  class UsersController < V1::ApplicationController
    allow_unauthenticated :authenticate, :create
    before_action :set_user, only: %i[update show]

    def show
      render json: @user
    end

    def update
      if @user.update(update_params)
        render json: @user
      else
        render_errors_for @user
      end
    end

    def create
      @user = User.new(create_params)
      if @user.save
        render json: @user
      else
        render_errors_for @user
      end
    end

    def authenticate
      @user = User.authenticate(auth_params)
      raise_unauthenticated unless @user
      Thread.current['current_user'] = @user
      render json: @user
    end

    private

    def set_user
      @user = User.find(params[:id])
      authorize_action_for @user
    end

    def user_params
      params.require(:user)
    end

    def create_params
      user_params.permit(:email, local_authentication_method_attributes: {
                           local_authentication_record_attributes: %i[password password_confirmation]
                         })
    end

    def update_params
      user_params.permit(:firstname, :lastname)
    end

    def auth_params
      params.permit(:id, user: %i[uid email password])
    end
  end
end
