# frozen_string_literal: true

module V1
  class SecurityContainersController < ApplicationController
    before_action :set_security_container, only: %i[show update destroy]
    before_action :set_security_containers, only: :index

    # GET /security_containers
    # GET /organizations/:organization_id/security_containers
    # GET /security_test_repositories/:security_test_repository_id/security_containers
    def index
      unless params[:test_name].nil?
        @security_containers = @security_containers.where('security_containers.name ILIKE ?', "%#{params[:test_name]}%")
      end
      render json: @security_containers,
             each_serializer: SecurityContainer.authorizer.serializer(current_user, in: @security_test_repository)
    end

    # GET /security_containers/:id
    def show
      render json: @security_container, serializer: @security_container.authorizer.serializer(current_user)
    end

    # This endpoint also supports creating multiple containers at once using an Array of Hashes like so:
    # {
    #   security_containers: [
    #     { ... },
    #     { ... },
    #   ]
    # }
    # POST /security_test_repositories/:security_test_repository_id/security_containers
    def create
      load_security_test_repository
      authorize_action_for SecurityContainer, in: @security_test_repository
      security_containers = @security_test_repository.security_containers.build(creation_params)
      create_and_collect_errors(security_containers)
    rescue ActiveRecord::RecordInvalid
      errors = security_containers.is_a?(Array) ? security_containers.map(&:errors) : security_containers.errors
      render json: { errors: errors }, status: :unprocessable_entity
    else
      render json: security_containers
    end

    # PUT /security_containers/:id
    def update
      if @security_container.update(container_params)
        render json: @security_container
      else
        render_errors_for @security_container
      end
    end

    # DELETE /security_containers/:id
    def destroy
      if @security_container.destroy
        head :no_content
      else
        render_errors_for @security_container
      end
    end

    private

    def create_and_collect_errors(security_containers)
      ActiveRecord::Base.transaction { Array(security_containers).each(&:save!) }
    end

    def set_security_container
      @security_container = SecurityContainer.find(params[:id])
      authorize_action_for @security_container
    end

    # Scoped load of security containers for the index listing. Three variations are possible:
    # 1. Scoped to an organization: Returns the containers for any whitelisted repositories in that organization plus
    #    any containers belonging to public repositories.
    # 2. Scoped to a repository: Returns the containers belonging to that repository. Attributes are redacted unless the
    #    user is an admin for the repository or the repository is whitelisted.
    # 3. Unscoped: Returns all containers belonging to the official repository
    def set_security_containers
      authorize_action_for SecurityContainer
      @security_containers =
        if load_organization
          available_containers_for_org(@organization)
        elsif load_security_test_repository
          @security_test_repository.security_containers
        else
          SecurityContainer.official_containers
        end
    end

    def load_organization
      return unless params[:organization_id]
      @organization = Organization.find(params[:organization_id])
    end

    def load_security_test_repository
      return unless params[:security_test_repository_id]
      @security_test_repository = SecurityTestRepository.find(params[:security_test_repository_id])
    end

    def available_containers_for_org(org)
      # To keep a consistent type for the @security_containers variable, coerce this into an
      # ActiveRecord query
      SecurityContainer.where(
        id: (org.whitelisted_containers.pluck(:id) + SecurityContainer.official_containers.pluck(:id))
      )
    end

    def creation_params
      if params[:security_containers]
        container_params_array
      else
        container_params
      end
    end

    def container_params_array
      params.require(:security_containers).map do |mapped_params|
        container_params(mapped_params)
      end
    end

    def container_params(target_params = params.require(:security_container))
      target_params
        .permit(:name, :category, :prog_args, :multi_host, :configurable, :help_url,
                :default_config, test_types: [], service: %i[name port transport_protocol]).tap do |whitelisted|
        whitelisted[:default_config] = target_params.delete(:default_config).tap do |config|
          config&.send(:permitted=, true)
        end
      end
    end
  end
end
