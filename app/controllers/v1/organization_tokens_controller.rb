# frozen_string_literal: true

module V1
  class OrganizationTokensController < V1::ApplicationController
    before_action :set_token, only: :update

    def update
      @token.recreate!
      render json: @token
    end

    private

    def set_token
      org = Organization.find(params[:organization_id])
      authorize_action_for org
      @token = org.organization_token
    end
  end
end
