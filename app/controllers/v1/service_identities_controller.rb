# frozen_string_literal: true

module V1
  class ServiceIdentitiesController < ApplicationController
    before_action :set_service, only: :create
    before_action :set_identity, only: %i[update show destroy]

    def update
      authorize_action_for @identity
      if @identity.update(identity_params)
        render json: @identity
      else
        render_errors_for(@identity)
      end
    end

    def show
      authorize_action_for @identity
      render json: @identity
    end

    def create
      authorize_action_for ServiceIdentity, in: @service.organization
      identity = @service.build_service_identity(identity_params)
      if identity.save
        render json: identity
      else
        render_errors_for(identity)
      end
    end

    def destroy
      authorize_action_for @identity
      @identity.destroy
      head :no_content
    end

    private

    def set_service
      @service = Service.find(params[:service_id])
    end

    def identity_params
      params.require(:service_identity).permit(:username, :password)
    end

    def set_identity
      @identity = ServiceIdentity.find(params[:id])
    end
  end
end
