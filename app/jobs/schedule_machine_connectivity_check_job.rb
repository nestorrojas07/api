# frozen_string_literal: true

require './lib/norad_exception_notifier'

class ScheduleMachineConnectivityCheckJob < ApplicationJob
  include NoradExceptionNotifier

  queue_as :swarm_scheduling

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)

    Resque.logger.error "Exception while starting job: #{exception.inspect}" if defined?(Resque)
    Resque.logger.error exception.backtrace.to_s if defined?(Resque)
  end

  def perform(check_id, relay_opts = {})
    check = MachineConnectivityCheck.find(check_id)
    check_full_path = "#{NORAD_REGISTRY}/#{check.class::CONTAINER_NAME}:#{check.class::CONTAINER_VERSION}"
    DockerSwarm.queue_machine_connectivity_container(
      check,
      GenericContainer.new(full_path: check_full_path),
      relay_opts
    )
  end
end
