# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  def scan_complete(docker_command_id)
    @docker_command = DockerCommand.find(docker_command_id)
    @organization = @docker_command.resolved_organization
    mail subject: "#{SUBJECT_PREFIX} Scan complete", bcc: @organization.users.pluck(:email)
  end

  helper do
    def services_url(machine)
      UiUrlHelper.ui_url v1_machine_services_url(machine)
    end

    def machines_url(organization)
      UiUrlHelper.ui_url v1_organization_machines_url(organization)
    end

    def duration_in_words(dc)
      distance_of_time_in_words(dc.started_at || dc.created_at, dc.finished_at || Time.now.utc)
    end

    def scan_url(dc)
      url =
        if dc.machine.present?
          "#{v1_machine_url(dc.machine)}/results?scan_id=#{dc.id}"
        else
          "#{v1_organization_url(dc.organization)}/results?scan_id=#{dc.id}"
        end
      UiUrlHelper.ui_url url
    end
  end
end
