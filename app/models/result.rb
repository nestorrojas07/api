# frozen_string_literal: true

# == Schema Information
#
# Table name: results
#
#  id                    :integer          not null, primary key
#  status                :integer          default("fail"), not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  sir                   :integer          default("unevaluated"), not null
#  assessment_id         :integer
#  ignored               :boolean          default(FALSE), not null
#  signature             :string           not null
#  reported              :boolean          default(FALSE), not null
#  title_encrypted       :string
#  description_encrypted :string
#  output_encrypted      :string
#  nid_encrypted         :string
#
# Indexes
#
#  index_results_on_assessment_id  (assessment_id)
#  index_results_on_signature      (signature)
#
# Foreign Keys
#
#  fk_rails_5e3335002f  (assessment_id => assessments.id) ON DELETE => cascade
#

class Result < ApplicationRecord
  # Vault Stuff
  include Vault::EncryptedModel
  vault_attribute :title
  vault_attribute :description
  vault_attribute :output
  vault_attribute :nid

  # Attribute Information
  enum status: %i[fail pass warn info error]
  enum sir: %i[unevaluated no_impact low medium high critical]

  API_ERROR_TITLE = 'API Error'
  API_ERROR_OUTPUT = 'An error occurred in the API while starting the scan. Please retry.'
  RELAY_ERROR_TITLE = 'Relay Error'
  RELAY_ERROR_OUTPUT = 'An error occured in the Relay while running the scan. '\
    'Please check the Relay logs for more information.'
  ASSESSMENT_TIMEOUT_TITLE = 'This assessment has timed out'
  ASSESSMENT_TIMEOUT_OUTPUT = 'It has been 48 hours since this assessment was scheduled. Please retry.'

  HUMAN_STATUSES = {
    fail: :failing,
    pass: :passing,
    error: :erroring,
    info: :informing,
    warn: :warning
  }.with_indifferent_access.freeze

  # Associations
  belongs_to :assessment, inverse_of: :results

  # Validations
  validates :status, presence: true
  validates :nid,
            presence: true,
            format: {
              with: /\A[A-Za-z][A-Za-z0-9\._-]*:[A-Za-z0-9\._-]+\z/,
              message: 'can only contain alphanumeric, dashes, and underscores'
            }
  validates :sir,
            inclusion: {
              in: sirs.keys,
              message: 'must be unevaluated, no_impact, low, medium, high, or critical'
            }
  validates :assessment, presence: true
  validates :signature,
            length: { is: 64 },
            format: {
              with: /\A[a-f0-9]+\z/, # case-insensitive pg indexes are cumbersome in rails, so enforce lowercase SHAs
              message: 'can only contain lowercase alphanumeric characters'
            }

  # Scopes
  scope(:not_ignored, -> { where(ignored: false) })
  scope(:passing, -> { where(status: statuses['pass']).not_ignored })
  scope(:failing, -> { where(status: statuses['fail']).not_ignored })
  scope(:warning, -> { where(status: statuses['warn']).not_ignored })
  scope(:informing, -> { where(status: statuses['info']).not_ignored })
  scope(:erroring, -> { where(status: statuses['error']).not_ignored })
  scope(:with_signature, ->(signature) { where(signature: signature) })

  def self.safe_create(attrs)
    return if attrs['status'] == 'pending'
    create! attrs
  rescue ArgumentError, ActiveRecord::RecordInvalid => exception
    create! ResultValidationErrorAttributes.new(exception).to_h
  end

  def self.create_parameter_error_result(assessment, exception)
    transaction do
      create! ResultMalformedParamsErrorAttributes.new(exception).to_h
      assessment.complete!
    end
  end

  def self.create_unexpected_error_result(assessment, exception)
    transaction do
      create! ResultUnexpectedErrorAttributes.new(exception).to_h
      assessment.reload
      assessment.complete!
    end
  end

  # Highly recommended to call this method with a filtered lists of results
  def self.ignore
    update_all(ignored: true)
  end

  # Highly recommended to call this method with a filtered lists of results
  def self.unignore
    update_all(ignored: false)
  end

  # Highly recommended to call this method with a filtered lists of results
  # Returns a hash like { failing: 0, passing: 5, erroring: 3, warning: 0, informing: 1 }
  def self.stats
    stats = not_ignored.group(:status).count(:status)
    HUMAN_STATUSES.each_with_object({}) do |(db_status, human_status), obj|
      obj[human_status] = stats.fetch(db_status.to_s, 0)
    end.with_indifferent_access
  end
end
