# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6ecd032ecb  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

class SshService < Service
  COMMON_SSH_PORTS = [22].freeze

  validate :encryption_type_is_ssh
  validate :port_type_is_tcp

  before_validation :set_service_defaults

  # Purposefully don't watch for any errors here; they're handled on the Service class.
  # Rails has an issue with callbacks, STI, and associations

  def keypair
    machine.ssh_key_pair
  end

  def service_configuration
    {
      port: port,
      ssh_user: organization.configuration.use_relay_ssh_key ? '%{ssh_user}' : machine.ssh_key_pair.try(:username),
      ssh_key: organization.configuration.use_relay_ssh_key ? '%{ssh_key}' : machine.ssh_key_pair.try(:key)
    }
  end

  private

  # Validation Definitions
  def encryption_type_is_ssh
    errors.add(:encryption_type, 'must be SSH') unless ssh?
  end

  def port_type_is_tcp
    errors.add(:port_type, 'must be TCP') unless tcp?
  end

  # Callback Definitions
  def set_service_defaults
    self.encryption_type = :ssh # must be SSH
    self.port_type = :tcp # must be TCP
    self.port = 22 if port.blank?
  end

  # For these special subclasses, we are going to look up the service type by name. If the user has
  # a nonstandard configuration, this is going to result in the port in the Service instance not
  # matching up with the associated ApplicationType instance, but that's OK.
  def set_application_type
    self.application_type = ApplicationType.find_by(name: 'ssh', port: 22, transport_protocol: 'tcp')
  end
end
