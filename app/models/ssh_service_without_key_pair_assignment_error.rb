# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class SshServiceWithoutKeyPairAssignmentError < OrganizationError
  include MachineAsErrable

  class << self
    include UncachedQuery

    def check(org, options = {})
      machine = options.fetch(:subject)
      if ssh_services_exists_for_machine?(machine) &&
         !key_pair_assignment_exists_for_machine?(machine) &&
         !org_configuration_uses_relay_ssh_key?(org)
        create_error(org, machine)
      else
        remove_error(org, machine)
      end
    end
  end

  def message
    "You have added an SSH Service to machine #{machine.name} but no SSH Key Pair is available to this machine."
  end
end
