# frozen_string_literal: true

class ScanSummary
  include ActiveModel::Model

  # RBAC
  include Authority::Abilities
end
