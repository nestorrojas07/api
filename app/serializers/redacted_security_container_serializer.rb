# frozen_string_literal: true

class RedactedSecurityContainerSerializer < ActiveModel::Serializer
  attributes :name
end
