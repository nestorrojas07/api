# frozen_string_literal: true

# == Schema Information
#
# Table name: security_test_repositories
#
#  id                 :integer          not null, primary key
#  host               :string
#  name               :string           not null
#  public             :boolean          default(FALSE)
#  username           :string
#  password_encrypted :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_security_test_repositories_on_host  (host)
#

class SecurityTestRepositoryCollectionSerializer < ActiveModel::Serializer
  attributes :name, :public, :official, :id
end
