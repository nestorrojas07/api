# frozen_string_literal: true

# == Schema Information
#
# Table name: notification_channels
#
#  id              :integer          not null, primary key
#  enabled         :boolean          default(TRUE)
#  event           :string
#  organization_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_notification_channels_on_event                      (event)
#  index_notification_channels_on_organization_id_and_event  (organization_id,event) UNIQUE
#

class NotificationChannelSerializer < ActiveModel::Serializer
  attributes :id, :enabled, :event, :description, :organization_id, :created_at, :updated_at

  def description
    NOTIFICATIONS[object.event]
  end
end
